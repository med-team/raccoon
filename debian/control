Source: raccoon
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>,
           Andreas Tille <tille@debian.org>
Section: contrib/science
XS-Autobuild: no
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-python,
               python3
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/med-team/raccoon
Vcs-Git: https://salsa.debian.org/med-team/raccoon.git
Homepage: http://autodock.scripps.edu/resources/raccoon

Package: raccoon
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
         autodocktools
Suggests: autodock,
          autodock-vina,
          torque
Description: preparation of in silico drug screening projects
 The field of computational biology is all about modeling
 physiochemical entities. Structural biology is about how
 those entities look in 3D and behave. Assume a specific
 behaviour (and only this) of a drug can be changed in a
 defined way.
 .
 Some good part of the characterisation of promising
 drug-like compounds for their interaction with a larger protein
 can be done on computers. This package helps with getting
 collections of small ligands prepared to be fitted against
 a particular protein of known structure. That docking itself
 is then to be performed by autodock or autodock-vina.
 .
 Raccoon may not find the drug, but it may well find a
 lead to it. To have this package with Debian shall help
 smaller biochemistry labs and grants an opportunity for the
 general public to educate itself and/or actively join in to
 help the world .... just a bit.
